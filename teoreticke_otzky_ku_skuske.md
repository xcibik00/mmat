# Seznam teoretických otázek ke zkoušce z MMAT
Vysvětlete daný pojem a uveďte k němu jednoduchý příklad:

## 1. **nezávislost vektorů**
Lineární kombinace vektorů v<sub>1</sub>, ... , v<sub>m</sub> je vektor 

**u = c<sub>1</sub>v<sub>1</sub> + ··· +c<sub>m</sub>v<sub>m</sub>**, kde c<sub>1</sub>,...,c<sub>m</sub> jsou konstanty.

Vektory v<sub>1</sub>, v<sub>2</sub>, ... , v<sub>m</sub> jsou **lineárně nezávislé**, jestliže rovnost 

**c<sub>1</sub>v<sub>1</sub> + c<sub>2</sub>v<sub>2</sub> + ··· + c<sub>m</sub>v<sub>m</sub> = o**, kde o je nulový vektor,

je splněna jedině v případě, že všechna čísla c<sub>1</sub>, ... , c<sub>m</sub> jsou nulová. V opačném případě jsou vektory **lineárně závislé**. \
Vektory jsou **lineárně závislé**, jestliže lze některý z nich vyjádřit jako lineární kombinaci ostatních. \
Speciálně: skupina vektorů obsahující nulový vektor je lineárně závislá.
**Pr.:**
```
u = (2,3)
v = (4,6)
w = (5,8)

vektory u a v su linearne zavisle pretoze v = 2*u
vektory u a w niesu linearne zavisle, pretoze sa nedaju odvodit od seba linearnou kombinaciou resp. rovnost plati.
```
## 2. **systém generátorů vektorového prostoru**
## 3. **báze vektorového prostoru**
## 4. **skalární součin** 
u · v = u<sub>1</sub>v<sub>1</sub> + ··· + u<sub>n</sub>v<sub>n</sub> (později bude obecněji zavedeno σ(u, v)), kde u = (u<sub>1</sub>, u<sub>2</sub>), v = (v<sub>1</sub>, v<sub>2</sub>) \
**Pr.:**
```
u = (1,5)
v = (3,6)

u · v = (1·3 + 5·6) = 33
```
## 5. **Gramova matice**

Matica ktorou sa da reprezentovat skalarny sucin. \
**σ(u,v) = uGv**, kde G = 

[ σ(e<sub>1</sub>, e<sub>1</sub>)   σ(e<sub>1</sub>, e<sub>2</sub>)     ...     σ(e<sub>1</sub>, e<sub>n</sub>) ] \
[ σ(e<sub>2</sub>, e<sub>1</sub>)   σ(e<sub>2</sub>, e<sub>2</sub>)     ...     σ(e<sub>2</sub>, e<sub>n</sub>) ] \
[ σ(e<sub>n</sub>, e<sub>1</sub>)   σ(e<sub>n</sub>, e<sub>2</sub>)     ...     σ(e<sub>n</sub>, e<sub>n</sub>) ] 

je tzv. Gramova matice daného skalárního součinu v bázi e.
Změna matice při přechodu k jiné bázi **f = eT** :
G′=T<sup>T</sup>GT

## 6. **lineární zobrazení mezi vektorovými prostory**
## 7. **vlastní vektor matice**

Mějme lineární zobrazení g : Rn → Rn s maticí A, g(u) = A u.
U některých vektorů se stane, že jejich obraz je násobkem původního vektoru (leží ve stejné přímce jako původní vektor): \
**Av=λv** \
Nenulový vektor v, pro který toto platí, nazveme **vlastním vektorem matice A** a hodnotu λ nazveme vlastní hodnotou neboli vlastním číslem matice A.
Vlastní čísla hledáme jako kořeny charakteristického polynomu \
**p(λ) = det(λI − A)**, neboli jako řešení rovnice \
**det(λI − A) = 0, případně det(A − λI) = 0.** \
Až najdeme vlastní čísla, hledáme vlastní vektory příslušné jednotlivým vlastním číslům jako řešení soustavy rovnic \
**(λI−A)v=o, případně (A−λI)v=o.** \
Vlastní vektory příslušné různým vlastním číslům jsou lineárně nezávislé.

## 8. **vlastní číslo matice**

Mějme lineární zobrazení g : Rn → Rn s maticí A, g(u) = A u.
U některých vektorů se stane, že jejich obraz je násobkem původního vektoru (leží ve stejné přímce jako původní vektor): \
**Av=λv** \
Nenulový vektor v, pro který toto platí, nazveme vlastním vektorem matice A a hodnotu λ nazveme vlastní hodnotou neboli **vlastním číslem matice A**.
Vlastní čísla hledáme jako kořeny charakteristického polynomu \
**p(λ) = det(λI − A)**, neboli jako řešení rovnice \
**det(λI − A) = 0, případně det(A − λI) = 0.** \
Až najdeme vlastní čísla, hledáme vlastní vektory příslušné jednotlivým vlastním číslům jako řešení soustavy rovnic \
**(λI−A)v=o, případně (A−λI)v=o.** \
Vlastní vektory příslušné různým vlastním číslům jsou lineárně nezávislé.

## 9. **kvadratická forma**
## 10. **tenzor**
Tenzor je reálná funkce několika vektorových argumentů, která má určité speciální vlastnosti. Příkladem tenzoru je například skalární součin, který dvojici vektorů přiřadí reálné číslo. Jiným příkladem tenzoru je determinant čtvercové matice, který opět několika vektorům, které tvoří sloupce dané matice, přiřadí reálné číslo
## 11. **lineární forma**
## 12. **duální prostor k vektorovému prostoru**
