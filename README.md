# MMAT 2019
## Prihlasovacie udaje
    * m: studentMMAT
    * h: MMAT2019
## Skripta

[skripta](http://www.umat.feec.vutbr.cz/~kovar/webs/personal/MMAT.pdf)\
[cvicenia-skripta](http://www.umat.feec.vutbr.cz/~kovar/webs/personal/MMATCV.pdf)

## Web

[Webová stránka](http://www.umat.feec.vutbr.cz/~kovar/webs/personal/html/mmat_login.php) doc. RNDr. Martina Kovára, Ph.D., garanta předmětu MMAT.
